﻿using System;
using System.Linq;

namespace arrays
{
    class Program
    {
        static void zadj1(){
            int [] arr = new int [10];
            for (int i=0 ; i<10 ; i++){
                arr[i]=i;
            }

            foreach (int i in arr)
            {
                Console.WriteLine(i);
            }
            
        }

        static void zadj2(){
            int [] arr = new int [10];
            for (int i=0 ; i<10 ; i++){
                arr[i]=9-i;
            }

            foreach (int i in arr)
            {
                Console.WriteLine(i);
            }
            
        }

        static void print_2d_array(int [,] arr){
            for (int i=0; i<arr.GetLength(0); i++){
                for (int j=0; j<arr.GetLength(1); j++){
                    Console.Write(arr[i,j]+" ");
                }
                Console.WriteLine();
            }
        }

        static int sum_2d_array(int [,] arr){
            int sum=0;
            for (int i=0; i<arr.GetLength(0); i++){
                for (int j=0; j<arr.GetLength(1); j++){
                    sum += arr[i,j];
                }
            }
            return sum;
        }

        static void zad1(){
            int [,] arr = new int [10,10];
            for (int i=0; i<10; i++){
                for (int j=0; j<10; j++){
                    if (i==j){
                        arr[i,j]=1;
                    } else{
                        arr[i,j]=0;
                    }
                }
            }
            print_2d_array(arr);
            int sum= sum_2d_array(arr);
            Console.WriteLine($"Suma elementów wynosi {sum}");
        }

        static void zad2(){
            int [,] arr = new int [10,10];
            for (int i=0; i<10; i++){
                for (int j=0; j<10; j++){
                    if (i==j){
                        arr[i,j]=i;
                    } else{
                        arr[i,j]=0;
                    }
                }
            }
            print_2d_array(arr);
            int sum= sum_2d_array(arr);
            Console.WriteLine($"Suma elementów wynosi {sum}");
        }

        static void zad3(){
            int [,] arr = new int [10,10];
            for (int i=0; i<10; i++){
                for (int j=0; j<10; j++){
                    if (i==9-j){
                        arr[i,j]=1;
                    } else{
                        arr[i,j]=0;
                    }
                }
            }
            print_2d_array(arr);
            int sum= sum_2d_array(arr);
            Console.WriteLine($"Suma elementów wynosi {sum}");
        }

        static void zad4(){
            int [,] arr = new int [10,10];
            for (int i=0; i<10; i++){
                for (int j=0; j<10; j++){
                    if (i==9-j){
                        arr[i,j]=i;
                    } else{
                        arr[i,j]=0;
                    }
                }
            }
            print_2d_array(arr);
            int sum= sum_2d_array(arr);
            Console.WriteLine($"Suma elementów wynosi {sum}");
        }

        static void zad5(){
            int [,] arr = new int [10,10];
            for (int i=0; i<10; i++){
                for (int j=0; j<10; j++){
                    if (j<=1){
                        arr[i,j]=Convert.ToInt32(Math.Pow(i,j+1));
                    } else {
                        arr[i,j]=0;
                    }
                }
            }
            print_2d_array(arr);
            int sum1=0;
            int sum2=0;
            for (int i=0; i<10; i++){
                sum1 += arr[i,0];
                sum2 += arr[i,1];
            }
            Console.WriteLine($"Suma elementów w kolumnie 1 wynosi {sum1}\nSuma elementów w kolumnie 1 wynosi {sum2}");
        }

        static void Main(string[] args)
        {
            //zadj1();
            //zadj2();
            //zad1();
            //zad2();
            //zad3();
            //zad4();
            zad5();
        }
    }
}
