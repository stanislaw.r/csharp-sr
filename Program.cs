﻿using System;

class Program
{
    static void Trojka(){
        Console.WriteLine("Przyprostokątna: ");
        int a=Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Przyprostokątna: ");
        int b=Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Przeciwprostokątna: ");
        int c=Convert.ToInt32(Console.ReadLine());

        if (a*a+b*b==c*c){
            Console.WriteLine("Trójka pitagorejska");
        } else{
            Console.WriteLine("Nie trójka pitagorejska");
        }
    }


    static void Pierwiastki1(){
        Console.WriteLine("a: ");
        int a=Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("b: ");
        int b=Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("c: ");
        int c=Convert.ToInt32(Console.ReadLine());

        double delta= b*b-4*a*c;

        if (delta==0){
                double x = (-b)/(2*a);
                Console.WriteLine("Pierwiastek: {0}",x);
        } else if (delta<0){
                Console.WriteLine("Brak pierwiastków");
        } else{
                double x1= (-b-Math.Sqrt(delta)/(2*a));
                double x2= (-b+Math.Sqrt(delta)/(2*a));
                Console.WriteLine("Pierwiastki: {0:0.00}, {1:0.00}",x1,x2);
        }
    }


    static void Pierwiastki2(){
        Console.WriteLine("a: ");
        int a=Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("b: ");
        int b=Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("c: ");
        int c=Convert.ToInt32(Console.ReadLine());

        double delta= b*b-4*a*c;

        switch (delta){
            case 0:
                double x = (-b)/(2*a);
                Console.WriteLine("Pierwiastek: {0}",x);
                break;
            
            case <0:
                Console.WriteLine("Brak pierwiastków");
                break;

            case >0:
                double x1= (-b-Math.Sqrt(delta)/(2*a));
                double x2= (-b+Math.Sqrt(delta)/(2*a));
                Console.WriteLine("Pierwiastki: {0:0.00}, {1:0.00}",x1,x2);
                break;
        }
    }


    static void Losowe(){
        Random rnd = new Random();
        bool win = false;
        int num = rnd.Next(0,10);
        int guess;
        while (!win){
            Console.WriteLine("Podaj liczbę: ");
            guess=Convert.ToInt32(Console.ReadLine());
            if (num==guess){
                Console.WriteLine("Zgadłeś!");
                win = true;
            } else {
                Console.WriteLine("Spróbuj ponownie!");
            }
        }
    }


    static void Main(string[] args)
    {
        //Trojka();
        //Pierwiastki1();
        //Pierwiastki2();
        //Losowe();
    }
}

