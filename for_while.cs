﻿using System;


class Program
{

    static void zad1(){
        for (int i=0; i<=10; i++){
            Console.WriteLine($"Dla x={i} równanie y=3x wynosi {3*i}");
        }
    }

    static void zad2(){
        int i=0;
        do{
            Console.WriteLine($"Dla x={i} równanie y=3x wynosi {3*i}");
            i++;
        }while(i<=10);
    }

    static void zad3(){
        for (int i=1; i<=20; i++){
            Console.Write($"{i}, ");
        }
    }

    static void zad4(){
        int i=1;
        do {
            Console.Write($"{i}, ");
            i++;
        } while (i<=20);
    }

    static void zad5(){
        int i=1;
        while (i<=20){
            Console.Write($"{i}, ");
            i++;
        }
    }

    static void zad6(){
        int sum=0;
        for (int i=1; i<=100; i++){
            sum+=i;
        }
        Console.WriteLine($"Suma liczb od 1 do 100 wynosi {sum}");
    }

    static void zad7(){
        int sum=0;
        int i=1;
        do{
            sum+=i;
            i++;
        } while (i<=100);
        Console.WriteLine($"Suma liczb od 1 do 100 wynosi {sum}");
    }

    static void zad8(){
        int sum=0;
        int i=1;
        while (i<=100){
            sum+=i;
            i++;
        }
        Console.WriteLine($"Suma liczb od 1 do 100 wynosi {sum}");
    }

    static void Main(string[] args)
    {
        //zad1();
        //zad2();
        //zad3();
        //zad4();
        //zad5();
        //zad6();
        //zad7();
        //zad8();
    }
}
