﻿using System;
class Program
{
    const double pi=3.14159;

    static void Vol(){
        Console.Write("Podaj promień kuli: ");
        double r=Convert.ToDouble(Console.ReadLine());

        double volume= (4/3)*pi*r*r*r;
        
        Console.WriteLine($"Objętość kuli o promieniu {r:0.00} jest równa {volume:0.00}");
    }

    static void Calk(){
        Console.Write("Podaj liczbę 1: ");
        int x=Convert.ToInt32(Console.ReadLine());
        Console.Write("Podaj liczbę 2: ");
        int y=Convert.ToInt32(Console.ReadLine());

        int div=x/y;

        Console.WriteLine($"Wynik dzielenia całkowitego {x} przez {y} to {div}");
    }

    static void Reszta(){
        Console.Write("Podaj liczbę 1: ");
        int x=Convert.ToInt32(Console.ReadLine());
        Console.Write("Podaj liczbę 2: ");
        int y=Convert.ToInt32(Console.ReadLine());

        int div=x%y;

        Console.WriteLine($"Reszta z dzielenia {x} przez {y} to {div}");
    }

    static void Dzialania(){
        Console.Write("Podaj liczbę 1: ");
        double x=Convert.ToDouble(Console.ReadLine());
        Console.Write("Podaj liczbę 2: ");
        double y=Convert.ToDouble(Console.ReadLine());

        double add= x+y;
        double sub= x-y;
        double mul= x*y;
        double div= x/y;

        Console.WriteLine($"{x} + {y} = {add}\n{x} - {y} = {sub}\n{x} * {y} = {mul:#.##}\n{x} / {y} = {div:#.##}");
    }

    static void Liniowe(){
        Console.Write("Podaj wartość a: ");
        double a=Convert.ToDouble(Console.ReadLine());
        Console.Write("Podaj wartość b: ");
        double b=Convert.ToDouble(Console.ReadLine());
        Console.Write("Podaj wartość c: ");
        double c=Convert.ToDouble(Console.ReadLine());

        if (a==0){
            if (b==c){
                Console.WriteLine("Równanie tożsamościowe");
            } else{
                Console.WriteLine("Równanie sprzeczne");
            }
        } else{
            double res=(c-b)/a;
            Console.WriteLine($"Dla a={a:#.##}, b={b:#.##}, c={c:#.##}, wartość x wynosi {res:#.##}");
        }
    }

    static void Losowe(){
        Random rnd = new Random();
        int num = rnd.Next(0,10);
        Console.WriteLine("Podaj liczbę: ");
        int guess=Convert.ToInt32(Console.ReadLine());
        if (num==guess){
            Console.WriteLine("Zgadłeś!");
        } else {
            Console.WriteLine($"Nie zgadłeś! Liczba wynosi {num}");
        }
    }

    static void Main(string[] args)
    {
        //Console.WriteLine($"Pi jest równe {pi}");
        //Console.WriteLine($"Kwadrat z pi jest równy {pi*pi:0.00}");
        //Vol();
        //Calk();
        //Reszta();
        //Dzialania();
        //Liniowe();
        //Losowe();
    }
}
